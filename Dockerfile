FROM mhart/alpine-node

RUN apk update && apk add yarn

RUN yarn global add serve

COPY build /opt/app/

CMD ["serve", "-p", "8080", "-s", "/opt/app/"]
