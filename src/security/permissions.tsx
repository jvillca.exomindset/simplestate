import Roles from 'src/security/roles';
import Storage from 'src/security/storage';

const storage = Storage.values;
const roles = Roles.values;

class Permissions {
  static get values() {
    return {
      investmentRead: {
        id: 'investmentRead',
        allowedRoles: [roles.admin],
      },
      investmentCreate: {
        id: 'investmentCreate',
        allowedRoles: [roles.admin],
      },
    };
  }

  static get asArray() {
    return Object.keys(this.values).map((value) => {
      return this.values[value];
    });
  }
}

export default Permissions;
