const backendUrl = `https://02ede33a-b196-40a7-87ea-40cc76ac4399.mock.pstmn.io/test`;

/**
 * Frontend Url.
 */
const frontendUrl = {
  host: 'localhost:3000',
  protocol: 'http',
};

export default {
  frontendUrl,
  backendUrl,
};
