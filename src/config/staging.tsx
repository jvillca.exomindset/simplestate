// Place the URL here with the /api suffix.
// Ex.:`https://domain.com/api`;
const backendUrl = `https://02ede33a-b196-40a7-87ea-40cc76ac4399.mock.pstmn.io/test`;

/**
 * Frontend URL.
 */
const frontendUrl = {
  host: 'simplestate.com',
  protocol: 'https',
};

export default {
  frontendUrl,
  backendUrl,
};
