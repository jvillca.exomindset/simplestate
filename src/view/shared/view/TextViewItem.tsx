import PropTypes from 'prop-types';
import React from 'react';
import { Typography } from '@material-ui/core';

function TextViewItem(props) {
  const value = `${props.prefix ? `${props.prefix} ` : ''}${
    props.value
  }`;

  return (
    <div style={{ marginBottom: '16px' }}>
      <Typography variant="overline" display="block" gutterBottom>
        {props.label}
      </Typography>
      <Typography variant="button" display="block" gutterBottom>{value}</Typography>
    </div>
  );
}

TextViewItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
  prefix: PropTypes.string,
};

export default TextViewItem;
