import React from 'react';
import InvestmentListToolbar from 'src/view/investment/list/InvestmentListToolbar';
import ContentWrapper from 'src/view/layout/styles/ContentWrapper';
import PageTitle from 'src/view/shared/styles/PageTitle';
import { i18n } from 'src/i18n';

function InvestmentListPage(props) {
  return (
    <>
      <ContentWrapper style={{ marginTop: '0px' }}>
        <PageTitle>{i18n('entities.investment.list.title')}</PageTitle>

        <InvestmentListToolbar />
      </ContentWrapper>
    </>
  );
}

export default InvestmentListPage;