import { Button, Divider, Grid, Typography } from '@material-ui/core';
import { getHistory } from 'src/modules/store';
import React, { useState } from 'react';
import { i18n } from 'src/i18n';
import FormWrapper, {
  FormButtons,
} from 'src/view/shared/styles/FormWrapper';
import { useForm, FormProvider } from 'react-hook-form';
import * as yup from 'yup';
import yupFormSchemas from 'src/modules/shared/yup/yupFormSchemas';
import { yupResolver } from '@hookform/resolvers';
import InputFormItem from 'src/view/shared/form/items/InputFormItem';
import actions from 'src/modules/investment/form/investmentFormActions';
import CurrencyFormItem from '../component/CurrencyFormItem';
import TypeInvestmentFormItem from '../component/TypeInvestmentFormItem';
import ConfirmModal from 'src/view/shared/modals/ConfirmModal';
import TextViewItem from 'src/view/shared/view/TextViewItem';
import { useDispatch, useSelector } from 'react-redux';
import selectors from 'src/modules/investment/form/investmentFormSelectors';
import { FilterButtons } from 'src/view/shared/styles/FilterWrapper';
import MaterialLink from '@material-ui/core/Link';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';

const schema = yup.object().shape({
  currencyId: yupFormSchemas.relationToOne(
    i18n('entities.investment.fields.currency'),
    { required: true },
  ),
  typeInvestmentId: yupFormSchemas.relationToOne(
    i18n('entities.investment.fields.typeOfInvestment'),
    { required: true },
  ),
  amount: yupFormSchemas.decimal(
    i18n('entities.investment.fields.amount'),
    {
      required: true,
      scale: 2,
      min: 0.01,
      max: 99999,
    },
  ),
});

function InvestmentForm(props) {
  const dispatch = useDispatch();

  const [initialValues] = useState(() => {
    return {
      currencyId: null,
      typeInvestmentId: null,
      amount: null
    };
  });

  const form = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: initialValues as any,
  });

  const doSimulate = (values) => {
    dispatch(actions.doSimulate({...values})); 
  };

  const doPayment = () => {
    dispatch(actions.doPayment());
  };

  const doCheckout = () => {
    dispatch(actions.doCheckout());
  };
  
  const currentStep = useSelector(selectors.selectCurrentStep);
  const simulatedInvestment = useSelector(selectors.selectSimulatedInvestment);
  const paymentInformation = useSelector(selectors.selectPaymentInformation);
  const loading = useSelector(selectors.selectLoading);
  const showConfirmModal = useSelector(selectors.selectShowConfirmModal);

  return (
    <FormWrapper>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(doSimulate)}>
          <Grid spacing={2} container>
            {currentStep !== 3 && (
              <>
                {currentStep === 1 && (
                  <>
                  <Grid item lg={6} xs={12}>
                    <TypeInvestmentFormItem
                      name="typeInvestmentId"
                      label={i18n(
                        'entities.investment.fields.typeOfInvestment',
                      )}
                      required={true}
                      showCreate={false}
                    />
                    <MaterialLink
                      style={{ marginBottom: '8px' }}
                      component={Link}
                    >
                      {i18n('entities.investment.moreInfo')}
                    </MaterialLink>
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <CurrencyFormItem
                      name="currencyId"
                      label={i18n(
                        'entities.investment.fields.currency',
                      )}
                      required={true}
                      showCreate={false}
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <InputFormItem
                        name="amount"
                        label={i18n(
                          'entities.investment.fields.amount',
                        )}
                        required={true}
                      />
                  </Grid>  
                  </>
                )}
                {currentStep === 2 && (
                  <Grid container spacing={2}>
                    <Grid item lg={6} xs={12}>
                      <TextViewItem
                        label={i18n('entities.investment.fields.investmentAmount')}
                        value={simulatedInvestment.amount}
                        prefix={'USD'}
                      />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                      <TextViewItem
                        label={i18n('entities.investment.fields.profitabilityAmount')}
                        value={simulatedInvestment.profitability_amount}
                        prefix={'USD'}
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Divider light />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.typeOfInvestment')}
                      value={''}
                    />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.profitability')}
                      value={simulatedInvestment.profitability + ' %'}
                    />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.montTerm')}
                      value={simulatedInvestment.mont_term}
                    />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.parking')}
                      value={simulatedInvestment.parking}
                    />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.amountReceiveTerm')}
                      value={simulatedInvestment.amount + simulatedInvestment.profitability_amount}
                      prefix={'USD'}
                    />
                    </Grid>
                    <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.fields.payment')}
                      value={simulatedInvestment.payment}
                    />
                    </Grid>
                  </Grid>
                )}
              </>
            )}
            {currentStep === 3 && (
              <Grid container spacing={2}>
                <Grid item lg={6} xs={12}>
                  <Typography variant="h5">
                    {i18n('entities.investment.subtitle')}
                  </Typography>
                </Grid>
                <Grid item lg={6} xs={12}>
                    <TextViewItem
                      label={i18n('entities.investment.amountToPay')}
                      value={simulatedInvestment.amount}
                      prefix={'USD'}
                    />
                </Grid>
                <Grid item lg={12} xs={12}>
                  <Typography variant="subtitle1">
                    {i18n('entities.investment.subtitle1')}
                  </Typography>
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.bank')}
                      value={paymentInformation.bank}
                    />
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.cuit')}
                      value={paymentInformation.cuit}
                    />
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.accountType')}
                      value={paymentInformation.account_type}
                    />
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.accountNumber')}
                      value={paymentInformation.account_number}
                    />
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.name')}
                      value={paymentInformation.name}
                    />
                </Grid>
                <Grid item lg={6} xs={12}>
                  <TextViewItem
                      label={i18n('entities.investment.fields.cbu')}
                      value={paymentInformation.cbu}
                    />
                </Grid>
                <Grid item lg={12} xs={12}>
                  <Typography variant="subtitle1">
                    {i18n('entities.investment.subtitle2')}
                  </Typography>
                </Grid>
                <Grid item lg={6} xs={12}>
                  <input accept="image/*" style={{display: 'none'}} id="icon-button-file" type="file" />
                  <label htmlFor="icon-button-file">
                    <IconButton color="primary" aria-label="upload picture" component="span">
                      <PhotoCamera />
                    </IconButton>
                  </label>
                </Grid>
              </Grid>
            )}
          </Grid>
          <FilterButtons>
            {currentStep === 1 && ( 
              <Button
                variant="contained"
                color="primary"
                disabled={loading}
                type="button"
                onClick={form.handleSubmit(doSimulate)}
                size="medium"
              >
                {i18n('common.continue')}
              </Button>
            )}
            {currentStep === 2 && ( 
              <Button
                variant="contained"
                color="primary"
                disabled={loading}
                type="button"
                onClick={doPayment}
                size="small"
              >
                {i18n('common.continue')}
              </Button>
            )}
            {currentStep === 3 && ( 
              <Button
                
                variant="contained"
                color="primary"
                disabled={loading}
                type="button"
                onClick={doCheckout}
                size="small"
              >
                {i18n('common.finish')}
              </Button>
            )}
          </FilterButtons>
        </form>
      </FormProvider>
      {showConfirmModal && (
        <ConfirmModal
          title={i18n('entities.investment.success')}
          onConfirm={() => getHistory().push('/investment')}
          onClose={() =>   getHistory().push('/investment')}
          okText={i18n('entities.investment.movements')}
          cancelText={i18n('common.cancel')}
        />
      )}
    </FormWrapper>
);
}

export default InvestmentForm;