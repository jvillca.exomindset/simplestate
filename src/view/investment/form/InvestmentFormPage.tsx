import React, { useState, useEffect } from 'react';
import ContentWrapper from 'src/view/layout/styles/ContentWrapper';
import PageTitle from 'src/view/shared/styles/PageTitle';
import Breadcrumb from 'src/view/shared/Breadcrumb';
import { i18n } from 'src/i18n';
import { getHistory } from 'src/modules/store';
import { useSelector, useDispatch } from 'react-redux';
import InvestmentForm from 'src/view/investment/form/InvestmentForm';
import actions from 'src/modules/investment/form/investmentFormActions';

function InvestmentFormPage() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.doInit());
  }, [dispatch]);

  return (
    <>
      <Breadcrumb
        items={[
          [i18n('entities.investment.menu'), '/investment'], 
          [i18n('entities.investment.new.title')]
        ]}
      />

      <ContentWrapper>
        <PageTitle>{i18n('entities.investment.new.title')}</PageTitle>

        <InvestmentForm
          onCancel={() => getHistory().push('/investment')}
        />
      </ContentWrapper>
    </>
  );
}

export default InvestmentFormPage;