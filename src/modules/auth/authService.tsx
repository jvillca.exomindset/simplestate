import authAxios from 'src/modules/shared/axios/authAxios';
import { AuthToken } from 'src/modules/auth/authToken';

export default class AuthService {

  static async signinWithEmailAndPassword(email, password) {

    const response = await authAxios.post('/login', {
      email,
      password,
    });

    return response.data;
  }

  static signout() {
    AuthToken.set(null, true);
  }
  
}
