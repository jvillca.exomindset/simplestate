import authAxios from 'src/modules/shared/axios/authAxios';

export default class CurrencyService {
  
  static async listAutocomplete(query, limit) {
    const params = {
      query,
      limit,
    };

    const response = await authAxios.get(
      `/getCurrencies`,
      {
        params,
      },
    );

    const data = Object.entries(response.data.data);
    const result = Object.keys(data).map(function(key){
      return { id: data[key][0] , label: data[key][1] };
    });
    return result;
  }
}
