import { createSelector } from 'reselect';

const selectRaw = (state) => state.investment.form;

const selectInvestment = createSelector(
  [selectRaw],
  (raw) => raw,
);

const selectInvestmentParams = createSelector(
  [selectInvestment],
  (investment) => (investment ? investment.investmentParams : null),
);

const selectSimulatedInvestment = createSelector(
  [selectInvestment],
  (investment) => (investment ? investment.simulatedInvestment : null),
);

const selectPaymentInformation = createSelector(
  [selectInvestment],
  (investment) => (investment ? investment.paymentInformation : null),
);


const selectShowConfirmModal = createSelector([selectRaw], (raw) =>
  Boolean(raw.showConfirmModal),
);

const selectLoading = createSelector([selectRaw], (raw) =>
  Boolean(raw.loading),
);

const selectCurrentStep = createSelector(
  [selectInvestment],
  (investment) => investment.step || 1,
);

const selectInitLoading = createSelector(
  [selectRaw],
  (raw) => Boolean(raw.initLoading),
);


const investmentFormSelectors = {
  selectInitLoading,
  selectInvestment,
  selectCurrentStep,
  selectInvestmentParams,
  selectSimulatedInvestment,
  selectPaymentInformation,
  selectLoading,
  selectShowConfirmModal,
  selectRaw,
};

export default investmentFormSelectors;