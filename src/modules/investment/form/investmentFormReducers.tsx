import actions from 'src/modules/investment/form/investmentFormActions';

const initialData = {
  step: 1,
  initLoading: false,
  loading: false,
  investmentParams: null,
  simulatedInvestment: null,
  paymentInformation: null,
  showConfirmModal: false
};

export default (state = initialData, { type, payload }) => {
  if (type === actions.INIT_STARTED) {
    return {
      ...state,
      step: 1,
      initLoading: true,
      showConfirmModal: false
    };
  }

  if (type === actions.INIT_SUCCESS) {
    return {
      ...state,
      initLoading: false,
    };
  }

  if (type === actions.INIT_ERROR) {
    return {
      ...state,
      initLoading: false,
    };
  }

  if (type === actions.SIMULATE_STARTED) {
    return {
      ...state,
      loading: true,
      investmentParams: payload,
    };
  }
  if (type === actions.SIMULATE_SUCCESS) {
    return {
      ...state,
      loading: false,
      step: 2,
      simulatedInvestment: payload.simulatedInvestment,
    };
  }
  if (type === actions.SIMULATE_ERROR) {
    return {
      ...state,
      loading: false,
      step: 1,
    };
  }

  if (type === actions.PAYMENT_STARTED) {
    return {
      ...state,
      loading: true,
    };
  }
  if (type === actions.PAYMENT_SUCCESS) {
    return {
      ...state,
      loading: false,
      step: 3,
      paymentInformation: payload.payment,
    };
  }
  if (type === actions.PAYMENT_ERROR) {
    return {
      ...state,
      loading: false,
      step: 1,
    };
  }

  if (type === actions.CHECKOUT_STARTED) {
    return {
      ...state,
      loading: true,
    };
  }
  if (type === actions.CHECKOUT_SUCCESS) {
    return {
      ...state,
      loading: false,
      showConfirmModal: true,
    };
  }
  if (type === actions.CHECKOUT_ERROR) {
    return {
      ...state,
      loading: false,
      step: 3,
      showConfirmModal: false
    };
  }

  return state;
};