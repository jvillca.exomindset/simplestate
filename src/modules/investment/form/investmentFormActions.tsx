import Errors from "src/modules/shared/error/errors";
import { getHistory } from 'src/modules/store';
import InvestmentService from 'src/modules/investment/investmentService';

const prefix = 'INVESTMENT_FORM';

const actions = {
  INIT_STARTED: `${prefix}_INIT_STARTED`,
  INIT_SUCCESS: `${prefix}_INIT_SUCCESS`,
  INIT_ERROR: `${prefix}_INIT_ERROR`,

  SIMULATE_STARTED: `${prefix}_SIMULATE_STARTED`,
  SIMULATE_SUCCESS: `${prefix}_SIMULATE_SUCCESS`,
  SIMULATE_ERROR: `${prefix}_SIMULATE_ERROR`,

  PAYMENT_STARTED: `${prefix}_PAYMENT_STARTED`,
  PAYMENT_SUCCESS: `${prefix}_PAYMENT_SUCCESS`,
  PAYMENT_ERROR: `${prefix}_PAYMENT_ERROR`,

  CHECKOUT_STARTED: `${prefix}_CHECKOUT_STARTED`,
  CHECKOUT_SUCCESS: `${prefix}_CHECKOUT_SUCCESS`,
  CHECKOUT_ERROR: `${prefix}_CHECKOUT_ERROR`,

  doInit: () => async (dispatch) => {
    try {
      dispatch({
        type: actions.INIT_STARTED,
      });

      dispatch({
        type: actions.INIT_SUCCESS,
      });
    } catch (error) {
      Errors.handle(error);
      dispatch({
        type: actions.INIT_ERROR,
      });
      getHistory().push('/investment');
    }
  },

  doSimulate: (values) => async (dispatch) => {
    try {
      dispatch({
        type: actions.SIMULATE_STARTED,
        payload: values,
      });
      
      const data = await InvestmentService.simulateInvestment(values);

      dispatch({
        type: actions.SIMULATE_SUCCESS,
        payload: {
          simulatedInvestment: data,
        },
      });

    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.SIMULATE_ERROR,
      });
    }
  },
  doPayment: () => async (dispatch) => {
    try {
      dispatch({
        type: actions.PAYMENT_STARTED,
      });
      
      const payment = await InvestmentService.getPayment();

      dispatch({
        type: actions.PAYMENT_SUCCESS,
        payload: {
          payment
        },
      });

    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.PAYMENT_ERROR,
      });
    }
  },
  doCheckout: () => async (dispatch) => {
    try {
      dispatch({
        type: actions.CHECKOUT_STARTED,
      });
      
      //await InvestmentService.storeInvestment();

      dispatch({
        type: actions.CHECKOUT_SUCCESS,
      });

    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.CHECKOUT_ERROR,
      });
    }
  }
};

export default actions;