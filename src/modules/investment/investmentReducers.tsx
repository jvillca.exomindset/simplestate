import list from 'src/modules/investment/list/investmentListReducers';
import form from 'src/modules/investment/form/investmentFormReducers';
import { combineReducers } from 'redux';

export default combineReducers({
  list,
  form,
});