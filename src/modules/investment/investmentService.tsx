import authAxios from 'src/modules/shared/axios/authAxios';

export default class InvestmentService {
  
  static async getPayment() { 
    const response = await authAxios.get(`/getPayment`);
    return response.data.data;
  }

  static async simulateInvestment(data) { 
    const body = {
      ...data,
    };

    const response = await authAxios.post(
      `/simulateInvestment`,
      body,
    );

    return response.data.data;
  }

  static async storeInvestment(data) { 
    const body = {
      ...data,
    };

    const response = await authAxios.post(
      `/storeInvestment`,
      body,
    );

    return response.data.data;
  }
}